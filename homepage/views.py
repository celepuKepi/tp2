from django.shortcuts import render
from donationList.models import Donation

# Create your views here.
def index (request) :
	news = Donation.objects.all()[:3]
	response = {'news':news}
	return render(request, 'index.html', response)