from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.
from datetime import datetime
from homepage.views import index

class TestURl(TestCase):
	def test_url_is_exist (self):
		responese = Client().get("")
		self.assertEqual(responese.status_code, 200)

	def test_using_index_function(self):
		found = resolve ('/')
		self.assertEqual(found.func, index)

	def test_if_using_index_template (self):
		responese = Client().get('/')
		self.assertTemplateUsed(responese, 'index.html')


