from django.test import TestCase, Client

class TestView(TestCase):
    def test_used_donations_html(self):
        response = self.client.get('/donations/')
        self.assertTemplateUsed(response, 'donations.html')