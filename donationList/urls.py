from django.urls import path

from donationList import views

urlpatterns = [
    path('',views.donations, name='donations'),
]