def variable_context(request):
    if 'img' in request.session.keys():
        return {'srcimage': request.session['img']}
    return {'srcimage': ''}