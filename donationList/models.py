from django.db import models

# Create your models here.

class Donation(models.Model):
    program = models.CharField(default='', max_length=30)
    target = models.DecimalField(max_digits=10, decimal_places=2,default=0)
    total = models.DecimalField(max_digits=10, decimal_places=2,default=0)
    news = models.TextField(default='')
    description = models.CharField(max_length=140, default='')
    picture = models.URLField()