from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from pro_story.models import DonationIn 
# from donationList.models import Donation

# Create your views here.
response = {}
def donation_history(request):
    try:
        donationHistory = DonationIn.objects.all().filter(email = request.user.email)
        response['donationHistory'] = donationHistory
        return render(request, 'donationHistory.html', response)
    except  AttributeError:
        return HttpResponseRedirect('/loginpage/')
       

