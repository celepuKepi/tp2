from django.apps import AppConfig


class ListDonationConfig(AppConfig):
    name = 'donationHistory'
