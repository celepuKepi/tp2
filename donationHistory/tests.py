from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from donationHistory.views import *
from app2.models import Signup

# Create your tests here.
class DonationHistoryURLTest(TestCase):
    def test_donationHistory_exist(self):
        page = resolve('/donationhistory/')
        self.assertEqual(page.func,donation_history)

class DonationHistoryViewTest(TestCase):
    def test_donationHistory_html_is_used(self):
        response = self.client.get('/donationhistory/')
        self.assertEqual(response.status_code, 302)


