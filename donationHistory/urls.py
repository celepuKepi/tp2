from django.urls import path
from django.contrib import admin
from django.contrib.auth import views
from donationHistory import views

urlpatterns = [
    path('',views.donation_history, name='donation_history'),
]