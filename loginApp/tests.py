from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse, resolve

from loginApp.views import user_logout


class TestLogin(TestCase):

    def setUp(self):
        User.objects.create(username='john', email='john@gmail.com', password='johntheripper')
        self.c = Client()

    def test_url_login_page(self):
        response = self.client.get('/loginpage/')
        self.assertEqual(response.status_code, 200)

    def test_login_redirect_to_google(self):
        response = self.client.get(reverse('social:begin', kwargs={'backend': 'google-oauth2'}))
        self.assertEqual(response.status_code, 302)

    def test_logout_redirect_to_home(self):
        response = self.client.get('/loginpage/logout/')
        reverseResponse = resolve('/loginpage/logout/')
        self.assertEqual(reverseResponse.func, user_logout)
        self.assertTrue(response.status_code, 302)

    def test_success_login_page_is_invalid(self):
        response = self.client.get('/loginpage/success/')
        self.assertEqual(response.status_code, 302)

    def test_success_login_by_user(self):
        session = self.client.session
        session['img'] = 'test'
        session.save()
        response = self.client.get('/loginpage/success/')

        self.assertEqual(response.status_code, 200)
