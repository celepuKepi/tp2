# dict_keys(['response', 'user', 'strategy', 'storage', 'backend', 'is_new', 'request', 'details', 'pipeline_index', 'uid', 'social', 'new_association', 'username'])

def get_img(request, *args, **kwargs):
    var = kwargs.get('response')
    request.session['img'] = var['image']['url']
