from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import logout




def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

def loginpage(request):
    return render(request, 'loginpage.html')

def success_login(request):
    if 'img' in request.session.keys():
        srcimage = request.session['img']
        return render(request, 'success_login.html', {'srcimage':srcimage})
    return HttpResponseRedirect('/')