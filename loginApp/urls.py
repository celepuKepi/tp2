from django.urls import path, include
from loginApp import views

urlpatterns = [
    path('success/', views.success_login),
    path('logout/', views.user_logout, name='user_logout'),
    path('', views.loginpage, name='loginpage')
]