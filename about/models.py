from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Komen(models.Model):
	nama = models.CharField(default='', max_length=30)
	komen = models.TextField(default='', max_length = 150)

	def as_dict(self):
		return {
			'nama' : self.nama,
			'komen' : self.komen,
		}

