
from django.urls import path

from about import views

urlpatterns = [
    path('',views.testimoni, name='testimoni'),
    path('api/', views.add_komen, name='add_komen'),
]