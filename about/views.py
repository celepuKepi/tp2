from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from about.models import *
from about.forms import *
from django.core import serializers

# Create your views here.
def testimoni(request):
    testi = Komen.objects.all()
    response = {'testi':testi}
    if request.method=="POST":
        form = KomenForm(request.POST)
        if form.is_valid():
            new_item = Komen(nama =request.POST['nama'], komen = request.POST['komen'])
            new_item.save()

            return HttpResponseRedirect('/about/')
    else:
        form = KomenForm()
    response['form'] = form
    return render(request, 'about.html', response)

def add_komen(request):
	comments = [ obj.as_dict() for obj in Komen.objects.all() ]
	data = {"comments" : comments }
	return JsonResponse(data, safe=False)
