import unittest
import time
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import testimoni, add_komen
from .forms import KomenForm
from .models import Komen 


# Create your tests here.
class Lab5UnitTest(TestCase):
	def test_url_is_exist (self):
		responese = Client().get("")
		self.assertEqual(responese.status_code, 200)

	def test_using_index_function(self):
		found = resolve ('/about/')
		self.assertEqual(found.func, testimoni)

	def test_if_using_index_template (self):
		responese = Client().get('/about/')
		self.assertTemplateUsed(responese, 'about.html')

	def test_if_models_return_dict(self):
		form = Komen.objects.create(nama="Ujang", komen="Mantappp")
		dicti = {
			"nama" : "Ujang",
			"komen" : "Mantappp",
		}
		self.assertEqual(form.as_dict(), dicti)

	# def test_forms(self):
	# 	form = Komen.objects.create(nama="Ujang", komen="Mantappp")
	# 	dicti = {
	# 		"nama" : "Ujang",
	# 		"komen" : "Mantappp",
	# 	}
	# 	form_data = {'testi' : 'testi' }
	# 	form = KomenForm(data=form_data)
	# 	self.assertTrue(form.is_valid())
	
	#test untuk inputan nya required
	def test_form_validation_for_blank_items(self):
		form = KomenForm(data={"nama" : "", "komen" : ""})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors["nama"],
			["This field is required."]
		)
	
	#ini untuk mencek apakah masuk database
	def test_model_in_database(self):
		# Creating a new activity
		new_activity = Komen.objects.create(nama = 'abeng ganteng', komen = 'skidipapap sawadikap' )
		# Retrieving all available activity
		counting_all_available_todo = Komen.objects.all().count()

		self.assertEqual(counting_all_available_todo, 1)