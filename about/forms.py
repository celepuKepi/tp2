from django import forms
from .models import *
from django.forms import ModelForm

class KomenForm(ModelForm):
	class Meta :
		model = Komen
		fields = ["nama", "komen"]

		widgets = {
			"nama" : forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'insert name here'}),
			"komen" : forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'insert testimony here'} )
		}
		