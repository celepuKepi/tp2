from django.urls import path
from pro_story import views

urlpatterns = [
    path('donate/',views.donationinput, name='donationinput'),
    path('savedonation/',views.savedonation, name='savedonation'),
    path('confirmation/',views.confirmation, name='confirmation'),
    path('<str:program>/',views.story, name='story'),
	path('<str:program>/news',views.news, name='news')
]
