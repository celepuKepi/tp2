from django import forms

PROGRAM_CHOICES = (
    ("Gempa 4,8 SR Terjadi di Lombok",("Gempa 4,8 SR Terjadi di Lombok")),
    ("Bantu Margie Lawan Kanker",("Bantu Margie Lawan Kanker")),
    ("Bantu Berikan Air Bersih",("Bantu Berikan Air Bersih")),
)

class DonationForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    attrs_checkbox = {
        'class': '',
    }
    program = forms.ChoiceField(choices=PROGRAM_CHOICES, label="Program")
    amount = forms.DecimalField(required=True, max_digits=10, widget=forms.TextInput(attrs=attrs))
    fullname = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    email = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    # donatur_showable = forms.BooleanField(label = 'Hide my name',required = False, widget = forms.CheckboxInput(attrs = attrs_checkbox))
