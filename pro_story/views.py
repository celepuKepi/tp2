from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.utils.datastructures import MultiValueDictKeyError

from donationList.models import Donation
from pro_story.forms import DonationForm
from pro_story.models import DonationIn
from app2.models import Signup

response = {}

# Create your views here.
def story(request, program):
    getProgram = Donation.objects.get(program=program)
    response = {'program':getProgram}
    return render(request, 'story.html', response)
def news(request,program):
	getProgram = Donation.objects.get(program = program)
	response = {'program' : getProgram}
	return render(request, 'news.html',response)

def donationinput(request):
    response['savedonation'] = DonationForm()
    return render(request,'donationinput.html',response)

def savedonation(request):
    form = DonationForm(request.POST or None)
    if(request.method == 'POST'):
        check_showable = request.POST.getlist('cekAnon')
        if(check_showable):
            response['fullname'] = request.POST['fullname']
        else:
            response['fullname'] = 'Anonymous'    
        response['program'] = request.POST['program']
        response['amount'] = request.POST['amount']
        response['email'] = request.POST['email']
        response['savedonation'] = form
        donate = DonationIn(
            program=response['program'],
            amount=response['amount'],
            fullname = request.POST['fullname'],
            email=response['email']
            )
        email_data = Signup.objects.all()
        for i in email_data:
            if i.Email == request.POST['email']:
                donate.save()
                return HttpResponseRedirect('/story/confirmation/')
        return HttpResponseRedirect('/register/member/')
    else:
        return HttpResponseRedirect('/story/<str:program>/')

def confirmation(request):
    confirm = DonationIn.objects.all()
    response['confirm'] = confirm
    return render(request,'confirmation.html',response)
